let datas =null;
function getData(url, cb) {
  let xhr = new XMLHttpRequest();
  xhr.onload = function () {
    if (xhr.status === 200) {
      return cb(JSON.parse(xhr.responseText));
    }
  };
  xhr.open("GET", url);
  xhr.send();
}

// datas = awaitgetData("https://jsonplaceholder.typicode.com/users", function(data) {
// 	datas = data;
//   console.log(datas)
// });
// console.log(datas)
export default getData;
import getData from "./data.js"
import Table from "./Table.js"
const message = 'List of Users' // Try edit me

// Update header text
document.querySelector('#header').innerHTML = message

const table = new Table({
  columns: ["ID","Name", "Username","Email","Address","Company"],
  data: []
});
const app = document.getElementById("app");
table.render(app);

setTimeout(function(){
  let datas = getData("https://jsonplaceholder.typicode.com/users/", function(datas) {
	  document.getElementById("loading").style.display="none";
    // console.log(datas);
    let [...tempArr] = table.init.data;
    let tempList=[];
    for(var i=0;i<datas.length;i++){
      // console.log("for",datas[i])
      let tempData = {
        id:datas[i].id,
        name:datas[i].name,
        uName:datas[i].username,
        email:datas[i].email,
        add:datas[i].address.street+","+datas[i].address.suite+","+datas[i].address.city,
        comp:datas[i].company.name
      };
      tempList.push(tempData);
      table.init.data.push(tempData);
      // console.log("dari for",tempList)
    }
    // console.log("dariluar",tempList)
    // console.log(table.init.data)
    // table.init.data = [...tempArr, tempList];
    // console.log(table.init.data)
    table.render(app);
  });
},1000)